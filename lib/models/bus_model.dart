

import 'package:bus_reservation/screens/bus/components/list.dart';

class BusModel {
  final String name;
  final String description;
  final String villeD;
  final String villeA;
  final String place;
  final String placeLibre;
  final String placeOccupe;
  final String date;
  final String price;

  BusModel(
    this.name,
    this.description,
    this.villeD ,
    this.villeA,
    this.place,
    this.placeLibre,
    this.placeOccupe,
    this.date,
    this.price
  );
}

// "Niloy Poribohon",
//   "Parboti Bus",
//   "Green Wheel",
//   "Chader Gari",
//   "Orleans Metropole",
//   "Marino Bus",
//   "VerBus",
//   "Setra Veiculo",
//   "Sotral"""

List<BusModel> busModel = [
  BusModel("${listBusName[0]}", "", "","", "", "", "", "", ""),
];