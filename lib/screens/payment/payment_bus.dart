import 'package:flutter/material.dart';

import '../../constants.dart';
import 'components/body.dart';

class Payment extends StatefulWidget {
  final int index;

  Payment(this.index);
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Payment",
            style: TextStyle(color: appBarColor, fontWeight: FontWeight.bold)),
        iconTheme: IconThemeData(color: appBarColor),
      ),
      body: Body(widget.index),
    );
  }
}
