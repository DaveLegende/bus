import 'package:bus_reservation/constants.dart';
import 'package:bus_reservation/screens/bus/components/list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class RightPaymentPanel extends StatelessWidget {
  final int index;

  RightPaymentPanel(this.index);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text("${listBusName[index]}", textScaleFactor: 1, style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Journey Start", style: TextStyle(color: Colors.grey[300])),
              Text("05May, 08:00am",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold))
            ]),
          ),
          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("From - To", style: TextStyle(color: Colors.grey[300])),
              Text("Dhaka to Bogoura",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold)),
            ]),
          ),

          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Pickup Point", style: TextStyle(color: Colors.grey[300])),
              Text("Haddenhurst Court (08:45)",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold)),
            ]),
          ),

          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Seat No", style: TextStyle(color: Colors.grey[300])),
              Text("C1 - C2 - C3",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold)),
            ]),
          ),
        ],
      ),
    );
  }
}