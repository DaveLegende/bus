import 'package:bus_reservation/constants.dart';
import 'package:bus_reservation/screens/bus/components/right_bus_panel.dart';
import 'package:swipe_up/swipe_up.dart';
import 'sliding_up.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:swipe_gesture_recognizer/swipe_gesture_recognizer.dart';

import 'list.dart';
import 'right_payment_panel.dart';

class Body extends StatefulWidget {
  final int index;

  Body(this.index);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool isSelect = false;
  int itemSelectionne1, itemSelectionne2;
  int value = 1;
  ScrollController _controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.width * 0.6;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: SingleChildScrollView(
        controller: _controller,
        child: Column(
          children: [
            Container(
              child: Card(
                color: Colors.transparent,
                elevation: 5,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: InkWell(
                        onTap: () {
                          // navigation(context, Payment());
                        },
                        child: Container(
                          height: height,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20.0),
                                bottomRight: Radius.circular(20.0)),
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Icon(AntDesign.qrcode, size: 70),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Container(
                        height: height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0),
                              bottomLeft: Radius.circular(20.0)),
                          color: Colors.white,
                        ),
                        child: RightPaymentPanel(widget.index),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              child: ListView.builder(
                  controller: _controller,
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context, index) => ListTile(
                        leading: Text("${listLeft[index]}"),
                        trailing: Text("${listRight[index]}"),
                      )),
            ),
            Container(
              child: TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: "Enter Voucher Code",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fillColor: Colors.white,
                  focusColor: Colors.white,
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              height: 50,
              child: Center(
                  child: IconButton(
                icon: Icon(AntDesign.up),
                onPressed: () {
                  showModalBottomSheet(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      context: context,
                      builder: (builder) => Container(
                            height: MediaQuery.of(context).size.width * 0.5,
                            padding: EdgeInsets.only(top: 5),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: Column(
                              children: [
                                ListTile(
                                  leading: _customRadio(),
                                  title: Text("MasterCard",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Text(
                                      "Pay for hjgdbvlx d<sl<gds sdush uihf",
                                      style:
                                          TextStyle(color: Colors.grey[300])),
                                  trailing: Image(
                                    image: AssetImage("assets/masterCard.png"),
                                    width: 30,
                                    height: 30,
                                  ),
                                ),
                                Divider(),
                                ListTile(
                                  leading: Radio(
                                      groupValue: itemSelectionne2,
                                      value: value,
                                      onChanged: (int i) {
                                        setState(() {
                                          itemSelectionne2 = i;
                                          isSelect = true;
                                        });
                                      }),
                                  title: Text("Visa",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Text(
                                      "Pay for hjgdbvlx d<sl<gds sdush uihf",
                                      style:
                                          TextStyle(color: Colors.grey[300])),
                                  trailing: Image(
                                    image: AssetImage("assets/visa.png"),
                                    width: 30,
                                    height: 30,
                                  ),
                                ),
                              ],
                            ),
                          ));
                },
              )),
            )
          ],
        ),
      ),
    );
  }

  Radio<int> _customRadio() {
    return Radio(
                                    groupValue: itemSelectionne1,
                                    value: value,
                                    onChanged: (int i) {
                                      setState(() {
                                        itemSelectionne1 = i;
                                      });
                                    });
  }
}
