import 'package:flutter/material.dart';


var listLeft = [
  "Seat - C1 - C2 - C3",
  "Discount",
  "Total"
];

var listRight = [
  "0.3X\$53",
  "25%",
  "\$63"
];

var image = [
  "assets/masterCard.png",
  "assets/visa.png",
];

var moyenPayment = ["Master Card", "Visa Card"];

var entete1 = ["Choose Classe", "Choose Price"];

var entete2 = ["Clear All", ""];

var buttonText = ["Niloy Poribolton", "Green Wheel", "Night Way"];

var isClicked = [true, false, false];

List<Widget> radio(int index) {
  int itemSelectionne;
  List<Widget> l = [];
  for (int x = 0; x < 3; x++) {
    Column column = Column(
      children: [
        ListTile(
          leading: Radio(
              groupValue: itemSelectionne,
              value: itemSelectionne,
              onChanged: (int i) {
                itemSelectionne = i;
              }),
          title:
              Text("${moyenPayment[index]}", style: TextStyle(fontWeight: FontWeight.bold)),
          subtitle: Text("Pay from ${moyenPayment[index]} account using ${moyenPayment[index]} payment gateway",
              ),
          trailing: Image(
            image: AssetImage("${image[index]}"),
            width: 30,
            height: 30,
          ),
        ),
        Divider(),
      ],
    );
    l.add(column);
  }
  return l;
}
