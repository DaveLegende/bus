import 'package:flutter/material.dart';
import 'package:bus_reservation/components/method_navigate.dart';
import '../../constants.dart';
import 'components/body.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar("Filter By"),
      body: Body(),
    );
  }

  AppBar _appBar(String title) {
    return AppBar(
      backgroundColor: Colors.white,
      title: Text(""+title, style: TextStyle(color: appBarColor, fontWeight: FontWeight.bold)),
      iconTheme: IconThemeData(
        color: appBarColor
      ),
    );
  }
}