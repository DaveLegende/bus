import 'package:flutter/material.dart';

import '../../../constants.dart';
import 'list.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int itemSelectionne;
  int itemSelectionne2;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Container(
                    child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Choose Class",
                            textScaleFactor: 1,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text("Clear All",
                              style: TextStyle(color: Colors.black))
                        ],
                      ),
                    ),
                    Row(
                      children: radio1(),
                    ),
                  ],
                )),
                Container(
                  child: Column(children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Choose Price",
                            textScaleFactor: 1,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text("", style: TextStyle(color: Colors.black))
                        ],
                      ),
                    ),
                    Row(
                      children: radio2(),
                    ),
                  ]),
                ),
                Container(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          padding: EdgeInsets.only(bottom: 10, left: 20),
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              "Choose Bus Services",
                              textScaleFactor: 1,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 20),
                          height: 25,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 3,
                            itemBuilder: (context, index) => Container(
                                child: RaisedButton(
                              color: isClicked[index]
                                  ? mbSecondebleuColorCpfind
                                  : mTextColor,
                              child: Text("${buttonText[index]}"),
                              onPressed: () {
                                setState(() {
                                  isClicked[index] = !isClicked[index];
                                });
                              },
                            )),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.only(left: 20),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: RaisedButton(
                                color: Colors.grey[300],
                                child: Text(
                                  "Parboti Bus",
                                  textScaleFactor: 1,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                onPressed: null,
                              ),
                            )),
                      ]),
                ),
              ],
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: RaisedButton(
                  color: appBarColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        "APPLY FILTER",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      )
                    ],
                  ),
                  onPressed: null,
                )),
          ],
        ),
      ),
    );
  }

  List<Widget> radio1() {
    List<Widget> l = [];
    for (int x = 0; x < 3; x++) {
      Row row = Row(
        children: [
          Radio(
            activeColor: appBarColor,
            value: x,
            groupValue: itemSelectionne,
            onChanged: (int i) {
              setState(() {
                itemSelectionne = i;
              });
            },
          ),
          Text("${list[x]}", style: TextStyle(fontWeight: FontWeight.bold)),
        ],
      );
      l.add(row);
    }
    return l;
  }

  List<Widget> radio2() {
    List<Widget> l = [];
    for (int x = 0; x < 3; x++) {
      Row row = Row(
        children: [
          Radio(
            activeColor: appBarColor,
            value: x,
            groupValue: itemSelectionne2,
            onChanged: (int i) {
              setState(() {
                itemSelectionne2 = i;
              });
            },
          ),
          Text("${list2[x]}", style: TextStyle(fontWeight: FontWeight.bold)),
        ],
      );
      l.add(row);
    }
    return l;
  }
}
