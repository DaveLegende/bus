import 'dart:async';

import 'package:bus_reservation/screens/bus/components/body.dart';
import 'package:bus_reservation/screens/filters/filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import '../../constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class BusScreen extends StatefulWidget {
  @override
  _BusScreenState createState() => _BusScreenState();
}

class _BusScreenState extends State<BusScreen> {
  @override
  Widget build(BuildContext context) {
    // Future.delayed(Duration.zero, () => _showDialog(context));
    return Scaffold(
      appBar: _appBar(context),
      body: Body(),
    );
  }

  AppBar _appBar(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(
          color: appBarColor
        ),
      backgroundColor: Colors.white,
      actions: [
        Container(
          padding: EdgeInsets.only(right: 10),
          child: IconButton(
            icon: Icon(Icons.search, color: appBarColor),
            onPressed: null,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 10),
          child: IconButton(
            icon: Icon(Entypo.sound_mix, color: appBarColor),
            onPressed: () {
              setState(() {
                Navigator.push(context, MaterialPageRoute(builder: (context) => FilterScreen()));
              });
            },
          ),
        )
      ]
    );
  }

  Future _showDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Container(
          height: MediaQuery.of(context).size.width * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SpinKitFadingCircle(color: appBarColor),
              Text("Please Wait"),
              Text(
                "The Systhem is waiting for the search result",
                maxLines: 2,
              ),
              RaisedButton(
                  child: Text("Ok"),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => BusScreen()));
                  })
            ],
          ),
        ),
      ),
    );
  }
}