import 'package:bus_reservation/components/method_navigate.dart';
import 'package:bus_reservation/screens/Details/bus_details.dart';
import 'package:bus_reservation/screens/plan/plan_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../constants.dart';

class RightBusPanel extends StatelessWidget {
  final int index;

  RightBusPanel(this.index);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(children: [
                IconButton(icon: Icon(AntDesign.barschart, color: appBarColor), onPressed: null),
                Text("AC", textScaleFactor: 1),
              ]),
              Row(children: [
                IconButton(
                    icon: Icon(MaterialIcons.directions_bus, color: appBarColor), onPressed: null),
                Text("2/1", textScaleFactor: 1),
              ]),
            ],
          )),
          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Icon(EvilIcons.clock),
              Text("Journey Start", style: TextStyle(color: Colors.grey[300])),
              Text("05May, 08:00am",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold))
            ]),
          ),
          Container(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Icon(EvilIcons.location),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text("From - To", style: TextStyle(color: Colors.grey[300])),
                  Text("Dhaka to Bogoura",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold))
                ]),
                Row(children: [
                  Text("\$",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)),
                  Text("58",
                      textScaleFactor: 1,
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold))
                ])
              ])
            ]),
          ),
        ],
      ),
    );
  }
}
