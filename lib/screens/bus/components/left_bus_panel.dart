import 'package:bus_reservation/screens/bus/components/list.dart';
import 'package:bus_reservation/constants.dart';
import 'package:flutter/material.dart';


class LeftBusPanel extends StatelessWidget {
  int index;

  LeftBusPanel({this.index});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
              flex: 2,
              child: Container(
                width: 100,
                height: 70,
                child: Image.asset("${listAssets[index]}", fit: BoxFit.cover),
              )),
          Expanded(
              flex: 1,
              child: Container(
                  child: Text("${listBusName[index]}",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold)))),
          Expanded(flex: 1, child: Container(child: Text("Agoé to Kégué"))),
          Expanded(
              flex: 1,
              child: Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 20,
                    // child: ListView.builder(
                    //     itemCount: 5,
                    //     itemBuilder: (context, index) =>
                    //         Icon(Icons.star, color: appBarColor)),
                  ),
                  Text("5.0")
                ],
              )))
        ],
      ),
    );
  }
}
