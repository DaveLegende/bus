import 'package:bus_reservation/components/method_navigate.dart';
import 'package:bus_reservation/models/custom_paint.dart';
import 'package:bus_reservation/screens/Details/bus_details.dart';
import 'package:bus_reservation/screens/plan/plan_bus.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';

import 'left_bus_panel.dart';
import 'middle_panel.dart';
import 'right_bus_panel.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  ScrollController _controller;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final height = MediaQuery.of(context).size.width * 0.5;
    return Container(
      child: ListView.builder(
        controller: _controller,
        shrinkWrap: true,
        itemCount: 9,
        itemBuilder: (context, index) => Container(
            padding: EdgeInsets.only(top: 5),
            child: Card(
              color: Colors.transparent,
              elevation: 5,
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: InkWell(
                      onTap: () {
                        navigation(context, BusDetails(index));
                      },
                      child: Container(
                        height: height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(20.0), bottomRight: Radius.circular(20.0), topLeft: Radius.circular(5), bottomLeft: Radius.circular(5)),
                          color: Colors.white,
                        ),
                        child: LeftBusPanel(index: index),
                      ),
                    ),
                  ),
                  // Expanded(
                  //   flex: 1,
                  //   child: FDottedLine(
                  //     color: Colors.black,
                  //     height: 160.0,
                  //     strokeWidth: 2.0,
                  //     dottedLength: 10.0,
                  //     space: 2.0,
                  //   ),
                  // ),
                  // Container(
                  //   height: height,
                  //   // child: Row(
                  //   //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //   //   children: [
                  //   //     LeftBusPanel(index: index),
                  //   //     MiddlePanel(),
                  //   //     RightBusPanel(),
                  //   //   ],
                  //   // ),
                  // ),
                  Expanded(
                    flex: 3,
                    child: InkWell(
                      onTap: () {
                        navigation(context, BusPlan(index));
                      },
                      child: Container(
                        height: height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), bottomLeft: Radius.circular(20.0), topRight: Radius.circular(5), bottomRight: Radius.circular(5)),
                          color: Colors.white,
                        ),
                        child: RightBusPanel(index),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
    );
  }
}
