import 'package:bus_reservation/components/method_navigate.dart';
import 'package:bus_reservation/constants.dart';
import 'package:bus_reservation/screens/bus/components/list.dart';
import 'package:bus_reservation/screens/bus/components/right_bus_panel.dart';
import 'package:bus_reservation/screens/payment/payment_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'svg_picture.dart';

class Body extends StatefulWidget {
  final int index;

  Body(this.index);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  ScrollController _controller = ScrollController();
  List<bool> isClicked = [false, false, true, false, true, true, false];
  // List<bool> isClick2 = [false, false, true, false, true, true, false];
  // List<bool> isClick3 = [false, false, true, false, true, true, false];
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.width * 0.5;
    return SingleChildScrollView(
      controller: _controller,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Card(
              color: Colors.transparent,
              elevation: 5,
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: InkWell(
                      onTap: () {
                        // navigation(context, Payment());
                      },
                      child: Container(
                        padding: EdgeInsets.only(left: 20),
                        height: height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                              topLeft: Radius.circular(5),
                              topRight: Radius.circular(20.0),
                              bottomRight: Radius.circular(20.0)),
                          color: Colors.white,
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                  child: Image.asset(
                                      "${listAssets[widget.index]}",
                                      fit: BoxFit.cover)),
                              Container(
                                  child: Text("${listBusName[widget.index]}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Container(
                                child: Row(
                                    children: [Icon(Icons.star), Text("5.0")]),
                              )
                            ]),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      height: height,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5),
                            topLeft: Radius.circular(20.0),
                            bottomLeft: Radius.circular(20.0)),
                        color: Colors.white,
                      ),
                      child: RightBusPanel(widget.index),
                    ),
                  )
                ],
              ),
            ),
          ),
          Card(
            elevation: 10,
            child: Container(
              padding: EdgeInsets.all(20),
              height: height * 3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(children: [red, Text("Sold Out")]),
                          Column(children: [black, Text("Available")]),
                          Column(children: [green, Text("Selected")]),
                          // SvgPicture.asset("assets/icons/sofa.svg", color: Colors.purple, height: 80, width: 80,)
                        ]),
                  ),
                  Expanded(
                    flex: 3,
                    child: GridView.builder(
                        controller: _controller,
                        gridDelegate:
                            new SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3),
                        itemCount: numerotation.length,
                        itemBuilder: (context, i) {
                          return Stack(
                            children: [
                              InkWell(
                                  onTap: () {
                                    setState(() {
                                      isClicked[i] = !isClicked[i];
                                    });
                                  },
                                  child: SvgPicture.asset(
                                      "assets/icons/black.svg",
                                      color: isClicked[i]
                                          ? Colors.green
                                          : Colors.black,
                                      height: 50,
                                      width: 50)),
                              Positioned(
                                  top: 10,
                                  left: 17,
                                  child:
                                      Center(child: Text("${numerotation[i]}")))
                            ],
                          );
                        }),
                  ),
                  // Expanded(
                  //   flex: 3,
                  //   child: ListView.builder(
                  //       controller: _controller,
                  //       shrinkWrap: true,
                  //       itemCount: 7,
                  //       itemBuilder: (context, index) => Row(
                  //               mainAxisAlignment:
                  //                   MainAxisAlignment.spaceBetween,
                  //               children: [
                  //                 Row(children: [
                  // Stack(
                  //   children: [
                  //     InkWell(
                  //         onTap: () {
                  //           setState(() {
                  //             isClick[index] =
                  //                 !isClick[index];
                  //           });
                  //         },
                  //         child: SvgPicture.asset(
                  //             "assets/icons/black.svg",
                  //             color: isClick[index]
                  //                 ? Colors.green
                  //                 : Colors.black,
                  //             height: 50,
                  //             width: 50)),
                  //     Positioned(
                  //         top: 10,
                  //         left: 17,
                  //         child: Center(
                  //             child: Text(
                  //                 "${numerotationLine1[index]}")))
                  //   ],
                  // ),
                  //                   SizedBox(width: 20),
                  //                   Stack(
                  //                     children: [
                  //                       InkWell(
                  //                           onTap: () {
                  //                             setState(() {
                  //                               isClick2[index] =
                  //                                   !isClick2[index];
                  //                             });
                  //                           },
                  //                           child: SvgPicture.asset(
                  //                               "assets/icons/black.svg",
                  //                               color: isClick2[index]
                  //                                   ? Colors.green
                  //                                   : Colors.black,
                  //                               height: 50,
                  //                               width: 50)),
                  //                       Positioned(
                  //                           top: 10,
                  //                           left: 17,
                  //                           child: Center(
                  //                               child: Text(
                  //                                   "${numerotationLine2[index]}")))
                  //                     ],
                  //                   ),
                  //                 ]),
                  //                 Stack(
                  //                   children: [
                  //                     InkWell(
                  //                         onTap: () {
                  //                           setState(() {
                  //                             isClick3[index] = !isClick3[index];
                  //                           });
                  //                         },
                  //                         child: SvgPicture.asset(
                  //                             "assets/icons/black.svg",
                  //                             color: isClick3[index]
                  //                                 ? Colors.green
                  //                                 : Colors.black,
                  //                             height: 50,
                  //                             width: 50)),
                  //                     Positioned(
                  //                         top: 10,
                  //                         left: 17,
                  //                         child: Center(
                  //                             child: Text(
                  //                                 "${numerotationLine3[index]}")))
                  //                   ],
                  //                 ),
                  //               ])),
                  // ),
                ],
              ),
            ),
          ),
          Container(
            child: RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              textColor: Colors.white,
              color: appBarColor,
              onPressed: () {
                navigation(context, Payment(widget.index));
              },
              child: Text("Confirm"),
            ),
          ),
        ]),
      ),
    );
  }
}
