import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

bool isClicked = false;
int index;

var red = SvgPicture.asset("assets/icons/red.svg",
    color: Colors.red, height: 50, width: 50);
var black = SvgPicture.asset(
  "assets/icons/black.svg",
  height: 50,
  width: 50,
);
var green = SvgPicture.asset(
  "assets/icons/green.svg",
  color: Colors.green,
  height: 50,
  width: 50,
);

var numerotation = [
  "A1",
  "A2",
  "A3",
  "B1",
  "B2",
  "B3",
  "C1",
  "C2",
  "C3",
  "D1",
  "D2",
  "D3",
  "E1",
  "E2",
  "E3",
  "F1",
  "F2",
  "F3",
  "G1",
  "G2",
  "G3"
];

// var numerotationLine2 = [
//   "A2",
//   "B2",
//   "C2",
//   "D2",
//   "E2",
//   "F2",
//   "G2"
// ];

// var numerotationLine3 = [
//   "A3",
//   "B3",
//   "C3",
//   "D3",
//   "E3",
//   "F3",
//   "G3"
// ];

var couch = Stack(
  children: [
    InkWell(
        onTap: () {
          isClicked = !isClicked;
        },
        child: SvgPicture.asset("assets/icons/black.svg",
            color: isClicked ? Colors.green : Colors.black,
            height: 50,
            width: 50)),
    Positioned(top: 10, left: 17, child: Center(child: Text("A1")))
  ],
);
