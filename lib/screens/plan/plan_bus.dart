import 'package:flutter/material.dart';

import '../../constants.dart';
import 'components/body.dart';

class BusPlan extends StatefulWidget {
  final int index;

  BusPlan(this.index);
  @override
  _BusPlanState createState() => _BusPlanState();
}

class _BusPlanState extends State<BusPlan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Bus Seat Paln", style: TextStyle(color: appBarColor, fontWeight: FontWeight.bold)),
        iconTheme: IconThemeData(
          color: appBarColor
        ),
      ),
      body: Body(widget.index),
    );
  }
}