import 'package:bus_reservation/components/method_navigate.dart';
import 'package:bus_reservation/screens/bus/components/list.dart';
import 'package:bus_reservation/screens/payment/payment_bus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../constants.dart';

class Body extends StatefulWidget {
  final int index;

  Body(this.index);
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String details = "Texte est issu du mot latin « textum », dérivé du verbe « texere » qui signifie « tisser ». Le mot s'applique à l'entrelacement des fibres utilisées dans le tissage, voir par exemple Ovide : « Quo super iniecit textum rude sedula Baucis = (un siège) sur lequel Baucis empressée avait jeté un tissu grossier »2 ou au tressage (exemple chez Martial « Vimineum textum = panier d'osier tressé »). Le verbe a aussi le sens large de construire comme dans « basilicam texere = construire une basilique » chez Cicéron3.Le sens figuré d'éléments de langage organisés et enchaînés apparaît avant l'Empire romain : il désigne un agencement particulier du discours. Exemple : « epistolas texere = composer des épîtres » - Cicéron (ier siècle av. J.-C.)4 ou plus nettement chez Quintilien (ier siècle apr. J.-C.) : « verba in textu jungantur = l'agencement des mots dans la phrase ";
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(children: [
          Container(
          height: MediaQuery.of(context).size.width * 0.8,
          color: Colors.white,
          margin: EdgeInsets.symmetric(vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text("${listBusName[widget.index]}",
                                    textScaleFactor: 1,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20)),
                                Container(
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(EvilIcons.clock),
                                        Text("Journey Start",
                                            style: TextStyle(
                                                color: Colors.grey[300])),
                                        Text("05May, 08:00am",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold))
                                      ]),
                                ),
                                Container(
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(EvilIcons.location),
                                        Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text("From - To",
                                                  style: TextStyle(
                                                      color: Colors.grey[300])),
                                              Text("Dhaka to Bogoura",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.bold))
                                            ]),
                                        Container(
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 20,
                                                // child: ListView.builder(
                                                //   shrinkWrap: true,
                                                //   scrollDirection: Axis.horizontal,
                                                //   itemCount: 5,
                                                //   itemBuilder: (context, index) => ListTile(
                                                //     leading: Icon(Icons.star, color: appBarColor),
                                                //   ),
                                                // ),
                                              ),
                                              Container(child: Text("5.0")),
                                            ],
                                          ),
                                        )
                                      ]),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                        child: Image(
                                      fit: BoxFit.fitWidth,
                                      image: AssetImage(
                                          "${listAssets[widget.index]}"),
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.4,
                                    )),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.topRight,
                                      child: Text("\$58"),
                                    ),
                                  ),
                                ]),
                          ),
                        ),
                      ])),
            ),
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10)),
                  color: Colors.grey[200],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      onPressed: null,
                      color: appBarColor,
                      child:
                          Text("Info", style: TextStyle(color: Colors.white)),
                    ),
                    RaisedButton(
                      onPressed: null,
                      color: Colors.grey[300],
                      child:
                          Text("Review", style: TextStyle(color: Colors.black)),
                    ),
                    RaisedButton(
                      onPressed: null,
                      color: Colors.grey[300],
                      child: Text("Pick Up Point",
                          style: TextStyle(color: Colors.black)),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      Container(
        height: MediaQuery.of(context).size.height * 0.7,
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("About ${listBusName[widget.index]}",
                textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),

            Text("$details", textAlign: TextAlign.justify),
            Text("Review",
                textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
            Text("4.8/5",
              textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)
            ),

            Text("Punctuality",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Icon(Icons.star, size: 10),
                        Icon(Icons.star, size: 10),
                        Icon(Icons.star, size: 10),
                        Icon(Icons.star, size: 10),
                        Icon(Icons.star, size: 10),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: RaisedButton(
                      color: appBarColor,
                      onPressed: () {
                        // navigation(context, Payment(widget.index));
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text("Submit", style: TextStyle(color: Colors.white)),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Align(alignment: Alignment.centerRight, child: Text("5.0")),
                  ),
                ],
              ),
            )
          ],
        ),
      )
    ])));
  }
}
