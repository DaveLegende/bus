import 'package:flutter/material.dart';

import '../../constants.dart';
import 'components/body.dart';

class BusDetails extends StatefulWidget {
  final int index;

  BusDetails(this.index);
  @override
  _BusDetailsState createState() => _BusDetailsState();
}

class _BusDetailsState extends State<BusDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Bus Details", style: TextStyle(color: appBarColor, fontWeight: FontWeight.bold)),
        iconTheme: IconThemeData(
          color: appBarColor
        ),
      ),
      body: Body(widget.index),
    );
  }
}