import 'package:flutter/material.dart';

import '../../../constants.dart';
import 'list_town.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  String text;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
     
        
          child:Column(
            children: [
              Container(
                padding: EdgeInsets.only(top: 5),
                width: MediaQuery.of(context).size.width * 0.8,
                height: MediaQuery.of(context).size.width * 0.5,
                child: Image.asset("assets/arretbus.jpg", fit: BoxFit.cover),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(

                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.date_range, color: appBarColor),
                          SizedBox(width: 30),
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("From",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  IconButton(
                                      icon: Icon(Icons.arrow_drop_down,
                                          color: appBarColor),
                                      onPressed: null),
                                ],
                              ),
                               Container(
                                  child: TextFormField(
                                    onChanged: (String string) {},
                                    decoration:
                                        InputDecoration(hintText: "Adidogomé"),
                                  ),
                                ),
                            
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              // //
              // Container(
              //   padding: EdgeInsets.symmetric(horizontal: 25),
              //   child: Column(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       Row(
              //         children: [
              //           Icon(Icons.place, color: appBarColor),
              //           SizedBox(width: 30),
              //           Column(
              //             children: [
              //               Row(
              //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                 children: [
              //                   Text("To",
              //                       style: TextStyle(
              //                           color: Colors.black,
              //                           fontWeight: FontWeight.bold)),
              //                   IconButton(
              //                       icon: Icon(Icons.arrow_drop_down,
              //                           color: appBarColor),
              //                       onPressed: null),
              //                 ],
              //               ),
              //               Expanded(
              //                 child: Container(
              //                   child: TextFormField(
              //                     onChanged: (String string) {},
              //                     decoration:
              //                         InputDecoration(hintText: "Adidogomé"),
              //                   ),
              //                 ),
              //               )
              //             ],
              //           ),
              //         ],
              //       ),
              //     ],
              //   ),
              // ),
              // //
              // Container(
              //   padding: EdgeInsets.symmetric(horizontal: 25),
              //   child: Column(
              //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //     children: [
                    // ListTile(
                    //   leading: Icon(Icons.date_range, color: appBarColor),
                    //   title: Text("Date of journey",
                    //       style: TextStyle(
                    //           color: Colors.black, fontWeight: FontWeight.bold)),
                    //   subtitle: DropdownButton(
                    //     value: text,
                    //     items: townList.map((valueItem) => DropdownMenuItem(value: valueItem, child: Text(valueItem)),
                    //     onChanged: (newText) {
                    //       setState(() {
                    //         text = newText;
                    //       });
                    //     },
                    //   ),
                    // ),
              //       Row(
              //         children: [
              //           Icon(Icons.date_range, color: appBarColor),
              //           SizedBox(width: 30),
              //           Column(
              //             children: [
              //               Row(
              //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //                 children: [
              //                   Text("Date of journey",
              //                       style: TextStyle(
              //                           color: Colors.black,
              //                           fontWeight: FontWeight.bold)),
              //                   IconButton(
              //                       icon: Icon(Icons.arrow_drop_down,
              //                           color: appBarColor),
              //                       onPressed: null),
              //                 ],
              //               ),
              //               Expanded(
              //                 child: Container(
              //                   child: TextFormField(
              //                     onChanged: (String string) {},
              //                     decoration:
              //                         InputDecoration(hintText: "Adidogomé"),
              //                   ),
              //                 ),
              //               )
              //             ],
              //           ),
              //         ],
              //       ),
              //     ],
              //   ),
              // ),
              Expanded(
                flex: 1,
                              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  child: RaisedButton(
                    color: appBarColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Confirm",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        )
                      ],
                    ),
                    onPressed: () {},
                  ),
                ),
              ),
            ],
          ),
      
   
    );
  }
}
