import 'dart:async';

import 'package:bus_reservation/screens/bus/bus_screen.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../../constants.dart';
import 'list_town.dart';

class BodyHome extends StatefulWidget {
  @override
  _BodyHomeState createState() => _BodyHomeState();
}

class _BodyHomeState extends State<BodyHome> {
  double age;
  String text;

  DateTime _date = DateTime.now();
  DateTime jour;

  //Pour les formulaires
  String villeD, villeA, date;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double padding = MediaQuery.of(context).size.width * 0.1;

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.width * 0.5,
            child: Image.asset("assets/arretbus.jpg", fit: BoxFit.cover),
          ),
          Container(
            // height: MediaQuery.of(context).size.height * 0.5,
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    child: ListTile(
                      leading: Icon(FontAwesome5.map, color: appBarColor),
                      // trailing: IconButton(
                      //     onPressed: null,
                      //     icon: Icon(Icons.arrow_drop_down, color: appBarColor)),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("From",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          IconButton(
                              onPressed: () {
                                DropdownMenuItem(child: Text("${townList[0]}"));
                              },
                              icon: Icon(Icons.arrow_drop_down,
                                  color: appBarColor)),
                        ],
                      ),
                      subtitle: TextFormField(
                        decoration: InputDecoration(hintText: "Adidogomé"),
                        validator: (String value){
                          if(value.isEmpty){
                            return 'Vous devez remplir ce champ';
                          }
                        },
                        onSaved: (String value) {villeD = value;},
                      ),
                    ),
                  ),
                  Container(
                    child: ListTile(
                      leading: Icon(Icons.place, color: appBarColor),
                      // trailing: IconButton(
                      //     onPressed: null,
                      //     icon: Icon(Icons.arrow_drop_down, color: appBarColor)),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("To",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          IconButton(
                              onPressed: null,
                              icon: Icon(Icons.arrow_drop_down,
                                  color: appBarColor)),
                        ],
                      ),
                      subtitle: TextFormField(
                        decoration: InputDecoration(hintText: "Kégué"),
                        validator: (String value){
                          if(value.isEmpty){
                            return 'Vous devez remplir ce champ';
                          }
                        },
                        onSaved: (String value) {villeA = value;},
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: padding),
                    child: ListTile(
                      leading: Icon(AntDesign.calendar, color: appBarColor),
                      // trailing: IconButton(
                      //     onPressed: null,
                      //     icon: Icon(Icons.arrow_drop_down, color: appBarColor)),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Date of Journey",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          IconButton(
                              onPressed: () {
                                montrerPicker();
                              },
                              icon: Icon(Icons.arrow_drop_down,
                                  color: appBarColor)),
                        ],
                      ),
                      subtitle: TextFormField(
                        readOnly: true,
                        onTap: () {montrerPicker();},
                          // keyboardType: TextInputType.datetime,
                          decoration: InputDecoration(hintText: (_date.toString())),
                          // validator: (String value){
                          //   if(value.isEmpty){
                          //     return 'Vous devez remplir ce champ';
                          //   }
                          // },
                          // onSaved: (String value) {date = value;},
                        ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: RaisedButton(
              color: appBarColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "Confirm",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  )
                ],
              ),
              onPressed: () {
                // if(!_formKey.currentState.validate()) {
                  
                // }
                if(_formKey.currentState.validate()) {
                  setState(() {
                  _showDialog(context);
                  Timer(Duration(seconds: 5), (){
                    Navigator.push(context,
                      MaterialPageRoute(builder: (context) => BusScreen()));
                  });
                });
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  Future _showDialog(BuildContext context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Container(
          height: MediaQuery.of(context).size.width * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SpinKitFadingCircle(color: appBarColor),
              Text("Please Wait"),
              Text(
                "The Systhem is waiting for the search result",
                maxLines: 2,
              ),

              // RaisedButton(
              //     child: Text("Ok"),
              //     onPressed: () {
              //       Navigator.push(context,
              //           MaterialPageRoute(builder: (context) => BusScreen()));
              //     })
            ],
          ),
        ),
      ),
    );
  }

  Future<Null> montrerPicker() async {
    DateTime choix = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2021),
        // lastDate: new DateTime.now(),
        lastDate: DateTime(2025),
        initialDatePickerMode: DatePickerMode.year);

    if(choix != null && choix !=_date) {
      setState(() => _date = choix);
    }
    // if (choix = null) {
    //   var difference = new DateTime.now().difference(choix);
    //   var jours = difference.inDays;
    //   var ans = (jours / 365);
    //   setState(() {
    //     age = ans;
    //   });
    // }
  }

  // String getText() {
  //   if(date == null){
  //     return _date.toString();
  //   }
  //   else {
  //     return '${jour.day}/${jour.month}/${jour.year}';
  //   }
  // }
}
