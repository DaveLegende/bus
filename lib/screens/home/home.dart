import 'package:bus_reservation/screens/home/components/body_home.dart';
import 'package:bus_reservation/screens/tickets/ticket.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter/material.dart';

import 'components/body_home copy.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int _currentIndex = 0;

  final tabs = [
    BodyHome(),
    MyTicket(),
    // Notification(),
    Center(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: tabs[_currentIndex],
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  BottomNavigationBar _bottomNavigationBar() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text("Home"),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesome.ticket),
          title: Text("My Tickets"),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.notifications),
          title: Text("Notificationns"),
        ),
      ],
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
    );
  }

  AppBar _appBar() {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(Icons.sort),
        onPressed: null,
      ),
      actions: [
        Container(
          padding: EdgeInsets.only(right: 20),
          child: ClipOval(
            child: Image(
              image: AssetImage("assets/appbar_image.jpg"),
              fit: BoxFit.cover,
              width: 55,
            ),
          )
        )
      ]
    );
  }
}
