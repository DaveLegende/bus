import 'package:bus_reservation/utile_size.dart';
import 'package:flutter/material.dart';

const mPrimaryColor = Color(0xFF008000);
const mPrimaryLightColor = Color(0xFFFFECDF);
//
const mPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFA573FF), Color(0xFF645AFF)],
);
//
const mbackgGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFF121016), Color(0xFF3F3C48)],
);
//
const mchateblueGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFF2DC9EB), Color(0xFF14D2B8)],
);
//
const mchateredGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFF15887), Color(0xFFFE9B86)],
);
//0xFF1C162E
const mbackgrondColor = Color(0xFF131315);
const mbackgrondColordashbord = Color(0xFF131315);
const mgreenColor = Color(0xFF14D2B8);
const mbtnfofaitColor = Color(0xFF50A1FF);
const mbredColor = Color(0xFFFF3A79);
const mbSeconderedColor = Color(0xFFFF2D55);
const mbSeconderedColorCpfind = Color(0xFFFF5F01);
const mbSecondebleuColorCpfind = Color(0xFF003E74);





const mbnotificationwritColor = Color(0xFF5A7BEF);
// const couleurViolet = Color(0xFF1C162E);

// const mbtnfofaitColor = Color(0xFF50A1FF);
const mBotomColor = Color(0xFF1C162E);
const mTextColor = Color(0xFF757575);

const mcardColor = Color(0xFFBEC2CE);
const mcardbtnColor = Color(0xFF242A37);


//
const appBarColor = Color(0xFF1C162E);
// const appFooterColor = Color(0xFF1C162E);

// liste des coloreur du profilr
const mColor1 = Color(0xFF50E3C2);
const mColor2 = Color(0xFF5856D6);
const mColor3 = Color(0xFFFFCC00);
const mColor4 = Color(0xFF5AC8FA);
const mColor5 = Color(0xFF7ED321);
const mColor6 = Color(0xFFFF8C00);
const mColor7 = Color(0xFF000000);
const mColor8 = Color(0xFF4CD964);
// edit profile backgrond color

const mColoredite = Color(0xFFF7F8FA);

// 
const couleurViolet2 = Color(0xFF6343FB);
const couleurVerte1 = Color(0xFF55E7B5);





const mAnimationDuration = Duration(milliseconds: 200);
//

//
// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String mEmailNullError = "Veuillez entrer votre email";
const String mInvalidEmailError = "Veuillez saisir un e-mail valide";
const String mPassNullError = "Veuillez entrer votre mot de passe";
const String mShortPassError = "Le mot de passe est trop court";
const String mMatchPassError = "Les mots de passe ne correspondent pas";
const String mNamelNullError = "S'il vous plaît entrez votre nom";
const String mPhoneNumberNullError =
    "Veuillez entrer votre numéro de téléphone";
const String mAddressNullError = "Veuillez entrer votre adresse";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: mTextColor),
  );
}

// pour les card des tuto
var cardAspectRatio = 12.0 / 16.0;
var widgetAspectRatio = cardAspectRatio * 1.2;
